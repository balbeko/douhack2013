window.stats = new Stats()
window.stats.domElement.style.position = 'absolute'
window.stats.domElement.style.bottom = '0px'
window.stats.domElement.style.zIndex = 100
document.body.appendChild(window.stats.domElement)

window.scene = new THREE.Scene()
window.camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 20000)
window.camera.position.set(0, 50, 400)
window.camera.lookAt(window.scene.position)
window.renderer = new THREE.WebGLRenderer
  antialias: true

window.controls = new THREE.TrackballControls(window.camera)

window.renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(window.renderer.domElement)

window.light = new THREE.PointLight(0xffffff)
window.light.position.set(0, 150, 0)
window.scene.add(window.light)


window.createsky = ->
  skyboxGeometry = new THREE.CubeGeometry(10000, 10000, 10000)
  skyboxMaterial = new THREE.MeshBasicMaterial
    color: 0x9999ff
    side: THREE.BackSide
  window.skyBox = new THREE.Mesh(skyboxGeometry, skyboxMaterial)
  # skyBox.flipSided = true
  window.scene.fog = new THREE.FogExp2(0x999900, 0.00025)
  window.scene.add(window.skyBox)

window.createsky()

jsonLoader = new THREE.JSONLoader()
jsonLoader.load "js/firstship.js", (geometry) ->
  material = new THREE.MeshLambertMaterial
    color: 0x8888ff
  spaceship = new THREE.Mesh(geometry, material)
  window.scene.add(spaceship)

###
window.ondeviceorientation = (event) ->
  accel_z = parseFloat((event.beta / 10).toFixed(1))
  accel_x = parseFloat((event.gamma / 10).toFixed(1))
  window.camera.position.x += accel_x
  window.camera.position.z += accel_z
  ###

render = ->
  requestAnimationFrame(render)
  window.renderer.render(window.scene, window.camera)
  window.stats.update()
  window.controls.update()
render()
