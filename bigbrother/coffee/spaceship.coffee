class window.ViewPort
  constructor: () ->
    @add_scene()
    @add_stats()
    @render()

  render: =>
    requestAnimationFrame(@render)
    @renderer.render(@scene, @camera)
    @stats.update()

  add_scene: =>
    @scene = new THREE.Scene()
    @camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1, 10000)
    @renderer = new THREE.WebGLRenderer()
    @renderer.setSize(window.innerWidth, window.innerHeight)
    @renderer.setClearColorHex(0xffffff, 1)
    document.body.appendChild(@renderer.domElement)

  add_stats: =>
    @stats = new Stats()
    @stats.domElement.style.position = 'absolute'
    @stats.domElement.style.top = '0px'
    document.body.appendChild(@stats.domElement)

  add_graphic: (graphic) =>
    @scene.add graphic

class window.Game
  constructor: ->
    @objects = []
    @asteroids = []
    @viewport = new ViewPort()
    setInterval(@update, 100)
  add_object: (object) =>
    @objects.push(object)
    for key, graphic of object.graphics
      @viewport.add_graphic(graphic)
  update: =>
    for object in @objects
      object.update()

class window.GameObject
  constructor: (@game, @x, @y, @z, model, @scale, @color) ->
    @graphics = {}
    @load(model)
    @camera = game.viewport.camera

  load: (model) =>
    jsonLoader = new THREE.JSONLoader()
    jsonLoader.load model, @load_callback
  load_callback: (geometry) =>
    material = new THREE.MeshLambertMaterial({color: @color})
    mesh = new THREE.Mesh(geometry, material)
    mesh.position.set(@x, @y, @z)
    mesh.scale.set(@scale, @scale, @scale)
    @graphics.mesh = mesh
    @after_load_callback()
    game.add_object(@)
  update: =>
    @position = @graphics.mesh.position
    @rotation = @graphics.mesh.rotation
    @graphics.mesh.geometry.computeBoundingSphere()
    @boundingRadius = @graphics.mesh.geometry.boundingSphere.radius

class window.Asteroid extends GameObject
  constructor: (game, x, y, z, @speed, scale) ->
    super(game, x, y, z, "js/asteroid.js", scale, 0x555555)

  after_load_callback: =>
    @graphics.mesh.name = "asteroid"
    @game.asteroids.push @

  update: =>
    @graphics.mesh.rotation.y += @speed.y
    @graphics.mesh.rotation.x += @speed.x
    @graphics.mesh.rotation.z += @speed.z
    super

class window.Spaceship extends GameObject
  constructor: (game, x, y, z, scale) ->
    @speed = 0
    @velocity = 0
    @pitch_angle = 0
    @roll_angle = 0
    @yaw_angle = 0

    super(game, x, y, z, "js/firstship.js", scale, 0xeeeeee)

  after_load_callback: =>
    @graphics.light = new THREE.DirectionalLight(0xffffff, 0.5)
    @graphics.light.target = @graphics.mesh
    
    @graphics.mesh.add @graphics.light
    @graphics.mesh.add @camera
    #@camera.lookAt(@graphics.mesh.position)

    @camera.position.set(0, 250, -500)

  update: =>
    @speed += @velocity
    @speed = 3 if @speed > 3
    @speed = 0 if @speed < 0

    @graphics.mesh.rotation.x = @pitch_angle
    @graphics.mesh.rotation.z = @roll_angle
    @graphics.mesh.rotation.y = @yaw_angle

    @graphics.mesh.translateZ(@speed)
    @camera.lookAt(@graphics.mesh.position)
    super
    @detectCollision()

  detectCollision: =>
    for asteroid in @game.asteroids
      dx = Math.pow(asteroid.position.x-@position.x, 2)
      dy = Math.pow(asteroid.position.y-@position.y, 2)
      dz = Math.pow(asteroid.position.z-@position.z, 2)
      d = Math.sqrt(dx + dy + dz)
      if d <= (asteroid.boundingRadius + @boundingRadius)
        @game.viewport.scene.remove(@graphics.mesh)
        @update = ->

  accel: (velocity) =>
    @velocity = velocity

  setParams: (data) =>
    @pitch_angle = - parseFloat(data.pitch) * Math.PI / 2
    @roll_angle = parseFloat(data.roll) * Math.PI / 2
    @yaw_angle = parseFloat(data.yaw) * Math.PI / 2
    @velocity = parseFloat(data.acceleration) * 4

window.game = new Game()

#window.game.viewport.camera.position.set(0, 200, -300)

for i in [1..25]
  x = Math.random() * 2000 - 1000
  y = Math.random() * 2000 - 1000
  z = Math.random() * 2000 - 1000

  scale = Math.random() * 2 + 1

  new Asteroid(game, x, y, z, {x: 0, y: 0, z: 0}, scale)
  # window.asteroid = new Asteroid(game, 0, 0, 550, {x: 0.0, y: 0.0, z: 0}, 2)
window.ship = new Spaceship(game, 0, 0, 0, 1)
#window.game.viewport.camera.position.set(0, 50, -300)
#window.game.viewport.camera.lookAt(window.ship)

window.socket = io.connect('http://172.29.8.117:9999')
window.socket.on 'playerControlState', (data) ->
  window.ship.setParams(data)
