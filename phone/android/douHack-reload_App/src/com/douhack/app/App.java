package com.douhack.app;

import com.badlogic.gdx.Game;
import com.douhack.app.entity.PlayerState;
import com.douhack.app.screen.ChooseShipScreen;
import com.douhack.app.screen.GameScreen;
import com.douhack.app.screen.LoadScreen;
import com.douhack.app.screen.MainMenuScreen;
import com.douhack.app.socket.SocketManager;

public class App extends Game {
	public LoadScreen loadScreen;
	public MainMenuScreen mainMenuScreen;
	public ChooseShipScreen chooseShipScreen;	
	public GameScreen gameScreen;
	public SocketManager socketMgr;
	
	public PlayerState playerState;
	
	@Override
	public void create() {
		loadScreen = new LoadScreen(this);
		mainMenuScreen = new MainMenuScreen(this);
		chooseShipScreen = new ChooseShipScreen(this);
		gameScreen = new GameScreen(this);
		
		socketMgr = new SocketManager(this);
		playerState = new PlayerState();
		//helpMenuScreen = new HelpMenuScreen(this);
		//gameScreen = new GameScreen(this);
		
		setScreen(loadScreen);		
	}
}
