package com.douhack.app.entity;

public class PlayerState {
/*
 *     "playerId" : "0",
    "yaw" : "-1 ... 1",
    "pitch" : "-1 ... 1",
    "roll" : "-1 ... 1",
    "acceleration" : "-1 ... 1",
    "shotRocketButtonState" : "true | false"
 */ private String shipId;
	private String playerName; 
	private String playerId;
	private float yaw;
	private float pitch;
	private float roll;
	
	private String shipRequest1;
	private String shipRequest2;
	
	private float acceleration;
	private boolean rocketFire;
	public String getPlayerName() {
		return "ANDROID";
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public float getYaw() {		
		return yaw;
	}
	public void setYaw(float yaw) {
		this.yaw = yaw;
	}
	public float getPitch() {
		return pitch;
	}
	public void setPitch(float pitch) {
		this.pitch = pitch;
	}
	public float getRoll() {
		return roll;
	}
	public void setRoll(float roll) {
		this.roll = roll;
	}
	public float getAcceleration() {
		return acceleration;
	}
	public void setAcceleration(float acceleration) {
		this.acceleration = acceleration;
	}
	public boolean isRocketFire() {
		return rocketFire;
	}
	public void setRocketFire(boolean rocketFire) {
		this.rocketFire = rocketFire;
	}
	
	public String getShipId() {
		return shipId;
	}
	
	public void setShipId(String shipId) {
		this.shipId = shipId;
	}
	public String getShipRequest1() {
		return shipRequest1;
	}
	public void setShipRequest1(String shipRequest1) {
		this.shipRequest1 = shipRequest1;
	}
	public String getShipRequest2() {
		return shipRequest2;
	}
	public void setShipRequest2(String shipRequest2) {
		this.shipRequest2 = shipRequest2;
	}
	
	
	
}
