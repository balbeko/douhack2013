package com.douhack.app.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.Table;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.TableLayout;
import com.douhack.app.App;

public class BaseScreen implements Screen {
	protected App app;
	
	protected Skin skin;
	protected Stage stage;
	protected Table table;
	
	public BaseScreen(App app) {
		this.app = app;
	}
	
	
	@Override
	public void render(float paramFloat) {
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.5f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);		
	}

	@Override
	public void resize(int paramInt1, int paramInt2) {

		
	}

	@Override
	public void show() {
		skin = new Skin(Gdx.files.internal("data/uiskin.json"), Gdx.files.internal("data/uiskin.png"));
		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);	
		table = new Table(skin);
		
		
		table.width = Gdx.graphics.getWidth();
		table.height = Gdx.graphics.getHeight();
		stage.addActor(table);
		
		
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {		
		stage.dispose();
		skin.dispose();		
		Gdx.input.setInputProcessor(null);
	}

}
