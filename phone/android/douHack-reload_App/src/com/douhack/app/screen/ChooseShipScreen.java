package com.douhack.app.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ClickListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.TableLayout;
import com.douhack.app.App;

public class ChooseShipScreen extends BaseScreen {
	public TextButton buttonShipOne;
	public TextButton buttonShipTwo;
	public ChooseShipScreen(App app) {
		super(app);
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);		
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 0.03f));
		stage.draw();
	}
	
	@Override
	public void show() {		
		super.show();
		System.out.println("CHOOSE");
		TableLayout layout = table.getTableLayout();		
			
		buttonShipOne = new TextButton("SHIP ONE", skin);
		buttonShipTwo = new TextButton("SHIP TWO", skin);
		buttonShipOne.setClickListener( new ClickListener() {
			
			@Override
			public void click(Actor actor, float x, float y) {
				app.socketMgr.bindShip(app.playerState.getShipRequest1());
				app.setScreen(app.gameScreen);	
				
			}
		});
		buttonShipTwo.setClickListener( new ClickListener() {
			
			@Override
			public void click(Actor actor, float x, float y) {
				app.socketMgr.bindShip(app.playerState.getShipRequest2());
				app.setScreen(app.gameScreen);		
				
			}
		});
		layout.register("shipOneButton", buttonShipOne);
		layout.register("shipTwoButton", buttonShipTwo);
		
		
		layout.parse(Gdx.files.internal("data/chooseShipLayout.txt").readString());
		Gdx.input.setInputProcessor(stage);
	}
	
	public void dispose() {
		super.dispose();
	}
	

}
