package com.douhack.app.screen;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ClickListener;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Slider.ValueChangedListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.TableLayout;
import com.douhack.app.App;


public class GameScreen extends BaseScreen {
	private final float SEND_TIME = 1f;
	private float sendDelta = 0f;
	
	private float acceleration = 0f;
	
	public GameScreen(App app) {
		super(app);
	}

	public void handleInput(float delta) {
		sendDelta+=delta;
		
		if(sendDelta > SEND_TIME) {
			
			if (Gdx.app.getType() == ApplicationType.Desktop) {
				if (Gdx.input.isKeyPressed( 21 )) { //<
					float yaw = app.playerState.getYaw();
					yaw += 0.01;
					app.playerState.setYaw(yaw);
				}
				if (Gdx.input.isKeyPressed( 22 )) { //>
					float yaw = app.playerState.getYaw();
					yaw -= 0.01;
					app.playerState.setYaw(yaw);
				}
				if (Gdx.input.isKeyPressed( 19 )) { //^
					float pitch = app.playerState.getPitch();
					pitch += 0.01;
					app.playerState.setPitch(pitch);
				}
				if (Gdx.input.isKeyPressed( 20 )) { //v
					float pitch = app.playerState.getPitch();
					pitch -= 0.01;
					app.playerState.setPitch(pitch);
				}
			}
			// ANDROID
			else if (Gdx.app.getType() == ApplicationType.Android) {
				float yaw = Gdx.input.getAccelerometerZ();
				float roll = Gdx.input.getAccelerometerX();
				float pitch = Gdx.input.getAccelerometerY();
				app.playerState.setYaw(yaw);
				app.playerState.setRoll(roll);
				app.playerState.setPitch(pitch);
			} else
				Gdx.app.exit();
						
					
			app.playerState.setAcceleration(acceleration);
			app.socketMgr.sendState();
			sendDelta = 0;
		}
		
		
		
	}

	public void handleUpdate(float delta) {
//		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 0.03f));
//		player.act(delta);
//		playerDragon.act(delta);
	}

	@Override
	public void render(float delta) {
		super.render(delta);		
		
		handleInput(delta);
		
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 0.03f));
		stage.draw();
	}
	


	@Override
	public void show() {
		super.show();
		TableLayout layout = table.getTableLayout();
		//start	
		Slider slider = new Slider(-1.0f, 1.0f, 0.01f, skin);
		slider.setValue(0);
		slider.setValueChangedListener( new ValueChangedListener() {
			
			@Override
			public void changed(Slider arg0, float arg1) {
				acceleration = arg1;
				
			}
		});
		
		TextButton fireButton = new TextButton("Fire", skin);
		fireButton.setClickListener( new ClickListener() {
			
			@Override
			public void click(Actor actor, float x, float y) {
				//check if connected;
				app.playerState.setRocketFire(true);	
				
			}
		});
		layout.register("fireButton", fireButton);
		layout.register("speedSlider", slider);
		
		layout.parse(Gdx.files.internal("data/gameLayout.txt").readString());
		Gdx.input.setInputProcessor(stage);
		
		
		
//		textureAtlas = new TextureAtlas(
//				Gdx.files.internal("data/texture_pack/pack"),
//				Gdx.files.internal("data/texture_pack/"));
//		joystick = new Joystick(60, 100, 50);
//		batch = new SpriteBatch();
//
//		playerDragon = new PlayerDragon();
//
//		fire = new ArrayList<Fireball>();
//		renderer = new ShapeRenderer();
//		player = new Plane(200, 200, Gdx.graphics.getWidth(),
//				Gdx.graphics.getHeight());
//		skin = new Skin(Gdx.files.internal("data/uiskin.json"),
//				Gdx.files.internal("data/uiskin.png"));
//		table = new Table(skin);
//		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
//				true);
//		table.width = Gdx.graphics.getWidth();
//		table.height = Gdx.graphics.getHeight();
//		table.align(Align.LEFT);
//		stage.addActor(table);
//
//		TableLayout layout = table.getTableLayout();
//		upButton = new TextButton("Up", skin);
//		layout.register("leftButton", upButton);
//
//		downButton = new TextButton("Down", skin);
//		layout.register("rightButton", downButton);
//
//		fireButton = new TextButton("Fire", skin);
//		fireButton.setClickListener(new ClickListener() {
//			@Override
//			public void click(Actor actor, float x, float y) {
//				// player.setY(player.getY()-1);
//				Fireball fireball = new Fireball(player.getX(), player.getY(),
//						Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
//						player.getAngle());
//				fire.add(fireball);
//			}
//		});
//		layout.register("fireButton", fireButton);
//
//		layout.parse(Gdx.files.internal("data/gameLayout.txt").readString());
//		Gdx.input.setInputProcessor(stage);
	}

}
