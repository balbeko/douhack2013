package com.douhack.app.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.douhack.app.App;

public class LoadScreen extends BaseScreen {
		
	private SpriteBatch batch;
	private Texture texture;
	private Sprite sprite;
	
	
	public LoadScreen(App app) {
		super(app);
	}

	
	public void render(float delta) {
		super.render(delta);		
	
		batch.begin();
		sprite.draw(batch);
		if (Gdx.input.justTouched()) {
			System.out.print(app.mainMenuScreen == null);
			app.setScreen(app.mainMenuScreen);
		}
				
		batch.end();
	}

		
	public void show() {
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();		

		batch = new SpriteBatch();
		
		texture = new Texture(Gdx.files.internal("data/libgdx.png"));
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		TextureRegion region = new TextureRegion(texture, 0, 0, 512, 275);
		
		sprite = new Sprite(region);

	}

	
	public void hide() {
		batch.dispose();
		texture.dispose();
	}

	
	public void pause() {
	}

	
	public void resume() {
	}

	
	public void dispose() {

		texture.dispose();
		
	}

}
