package com.douhack.app.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ClickListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.Table;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.TableLayout;
import com.douhack.app.App;

public class MainMenuScreen extends BaseScreen {
	
	public MainMenuScreen(App app) {
		super(app);
	}	
		
	@Override
	public void render(float delta) {
		super.render(delta);		
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 0.03f));
		stage.draw();
	}

	public void show() {
		super.show();
		TableLayout layout = table.getTableLayout();
		//start		
		TextButton button = new TextButton("Connect!", skin);
		button.setClickListener( new ClickListener() {
			
			@Override
			public void click(Actor actor, float x, float y) {
				//check if connected;
				boolean isSocketConnected = app.socketMgr.connect(null);
				if (isSocketConnected) {
					app.socketMgr.requestShip();
					app.setScreen(app.chooseShipScreen);
				} else {
					Gdx.app.log("SOCKET", "Socket cant connect");
				}
				
			}
		});
		layout.register("startButton", button);
		
		layout.parse(Gdx.files.internal("data/mainMenuLayout.txt").readString());
		Gdx.input.setInputProcessor(stage);
	}


	public void dispose() {
		super.dispose();
	}

}
