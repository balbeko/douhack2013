package com.douhack.app.socket;

import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIOException;

import org.json.JSONException;
import org.json.JSONObject;

import com.badlogic.gdx.Gdx;

public class SocketHandler implements IOCallback {

	@Override
	public void on(String event, IOAcknowledge arg1, Object... arg2) {
		if (event.equalsIgnoreCase("vibrate")) {
			Gdx.input.vibrate(2000);
		}

	}

	@Override
	public void onConnect() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDisconnect() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onError(SocketIOException arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMessage(String arg0, IOAcknowledge arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMessage(JSONObject json, IOAcknowledge ack) {
		try {
			System.out.println("Server said:" + json.toString(2));			
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

}
