package com.douhack.app.socket;

import io.socket.IOAcknowledge;
import io.socket.SocketIO;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.douhack.app.App;

public class SocketManager {
	
	private App app;
	private final String DEFAULT_ADDRESS = "http://172.29.8.117:9999";
	private SocketIO socket;
	
	public SocketManager(App app) {
		this.app = app;
		socket = new SocketIO();
	}
	
	public boolean connect(String url) {
		
		
		SocketHandler socketHandler = new SocketHandler();
		try {
			
			socket.connect(DEFAULT_ADDRESS,socketHandler);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void sendState() {
	   /* 
	    * 3. name: setPlayerControlState
		initiator: mobile
		requestData:
		{
	    * "playerId" : "0",
	    "yaw" : "-1 ... 1",
	    "pitch" : "-1 ... 1",
	    "roll" : "-1 ... 1",
	    "acceleration" : "-1 ... 1",
	    "shotRocketButtonState" : "true | false" */
		JSONObject json = new JSONObject();
		try {			
			json.put("timestamp", System.currentTimeMillis());
			json.put("playerId", app.playerState.getPlayerId());
			json.put("yaw", String.format("%.2f",  app.playerState.getYaw() ));
			json.put("pitch", String.format("%.2f",  app.playerState.getPitch()) );
			json.put("roll", String.format("%.2f",  app.playerState.getRoll() ));
			json.put("acceleration", String.format("%.2f",   app.playerState.getAcceleration() ));
			json.put("shotRocketButtonState", app.playerState.isRocketFire());
			app.playerState.setRocketFire(false);
			
			System.out.println("Server said:" + json.toString(2));			
			
			socket.emit("setPlayerControlState", json);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public void requestShip() {
		IOAcknowledge acknowledge = new IOAcknowledge() {
			
			@Override
			public void ack(Object... arg0) {
				List listAck = Arrays.asList( arg0 ) ;			
				for (Object obj: listAck) {
					JSONObject json = (JSONObject) obj;
					System.out.println(obj.toString());
				
					try {
						List list = Arrays.asList( json.getJSONArray("Ships") );
						int i = 0;
						for( Object obj2: list) {
							JSONObject json2 = (JSONObject)obj2;
							String id = json2.getString("shipId");
							if (i == 0 ) {
								if (json2.getString("occupied").equalsIgnoreCase("true"))  {
									app.chooseShipScreen.buttonShipOne.touchable  = false;
								} else
									app.chooseShipScreen.buttonShipOne.touchable  = true;
								app.playerState.setShipRequest1(id);
							}
							if (i == 1 ) {
									if (json2.getString("occupied").equalsIgnoreCase("true"))  {
										app.chooseShipScreen.buttonShipTwo.touchable  = false;
									} else
										app.chooseShipScreen.buttonShipTwo.touchable  = true;
									app.playerState.setShipRequest2(id);
							}
							
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
		};
		JSONObject json = new JSONObject();
		socket.emit("getAvailableShips",acknowledge, json);
		System.out.println(acknowledge);
	}
	
	public void bindShip(String shipId) {
		IOAcknowledge acknowledge = new IOAcknowledge() {
			
			@Override
			public void ack(Object... arg0) {
				List list = Arrays.asList( arg0 ) ;
			
				for (Object ob: list) {
					JSONObject json = (JSONObject) ob;
					System.out.println(json.toString());
					String result;
					String playerId;
					try {
						result = json.getString("result");
						if ( (result != null) & ( result.equals("ok"))  ){						
							playerId = json.getString("playerId");
							if (playerId != null) {
								app.playerState.setPlayerId(playerId);
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
				}
				
			}
		};
		JSONObject json = new JSONObject();
		try {
			json.put("shipId", "0");
			json.put("playerName", app.playerState.getPlayerName());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		socket.emit("bindWithShip", acknowledge, json);

	}
}
