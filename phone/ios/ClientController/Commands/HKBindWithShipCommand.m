////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  HKBindWithShipCommand.m
//  ClientController
//
//  Created by GregoryM on 3/31/13.
//  Copyright 2013 Hackaton. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Imports

#import "HKBindWithShipCommand.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private interface

@interface HKBindWithShipCommand ()
@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation

@implementation HKBindWithShipCommand

#pragma mark -
#pragma mark Properties

#pragma mark -
#pragma mark Initialization & Release

- (id)init
{
    self = [super init];
    if (nil != self)
    {
        self.name = @"bindWithShip";
    }
    return self;
}

- (void)prepareRequestData
{
    self.requestData = @{@"shipId": [NSString stringWithFormat:@"%d", self.shipId],
                         @"playerName": self.playerName};
}

- (NSUInteger)playerId
{
    if (self.executed && self.responseData != nil)
    {
        return [self.responseData[@"playerId"] integerValue];
    }
    return 0;
}

- (BOOL)result
{
    if (self.executed && self.responseData != nil)
    {
        return [self.responseData[@"result"] isEqualToString:@"ok"];
    }
    return NO;
}

#pragma mark -
#pragma mark Public methods

#pragma mark -
#pragma mark Private methods

@end