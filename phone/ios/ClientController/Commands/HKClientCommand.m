////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  HKClientCommand.m
//  ClientController
//
//  Created by GregoryM on 3/31/13.
//  Copyright 2013 Hackaton. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Imports

#import "HKClientCommand.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private interface

@interface HKClientCommand ()

@property (nonatomic, assign) BOOL executed;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation

@implementation HKClientCommand

#pragma mark -
#pragma mark Properties

#pragma mark -
#pragma mark Initialization & Release

- (id)init
{
    self = [super init];
    if (nil != self)
    {
        self.requestData = [NSDictionary dictionary];
        self.executed = NO;
    }
    return self;
}

#pragma mark -
#pragma mark Public methods

- (void)prepareRequestData
{
}

- (void)sendCommandInSocket:(SocketIO *)aSocket withBlock:(void(^)(HKClientCommand *command))aBlock
{
    NSLog(@"sending data: %@", self.requestData);
    
    [self prepareRequestData];
    
    [aSocket sendEvent:self.name withData:self.requestData andAcknowledge:^(id argsData)
     {
         if ([argsData isKindOfClass:[NSString class]])
         {
             self.responseData = [NSJSONSerialization JSONObjectWithData:[(NSString *)argsData dataUsingEncoding:NSUTF8StringEncoding]
                                                                        options:(NSJSONReadingOptions)0
                                                                          error:nil];
         }
         NSLog(@"command response arrived: %@", self.responseData);
         
         self.executed = YES;
         
         if (aBlock != nil)
         {
             aBlock(self);
         }
     }];
}

#pragma mark -
#pragma mark Private methods

@end