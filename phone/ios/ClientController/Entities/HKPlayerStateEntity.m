////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  HKPlayerStateEntity.m
//  ClientController
//
//  Created by GregoryM on 3/31/13.
//  Copyright 2013 Hackaton. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Imports

#import "HKPlayerStateEntity.h"
#import "HKPlayerEntity.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private interface

@interface HKPlayerStateEntity ()
@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation

@implementation HKPlayerStateEntity

#pragma mark -
#pragma mark Properties

#pragma mark -
#pragma mark Initialization & Release

- (id)init
{
    self = [super init];
    if (nil != self)
    {
    }
    return self;
}

- (NSDictionary *)dictionary
{
    return @{@"playerId": [NSString stringWithFormat:@"%u", self.playerId],
             @"yaw" : [NSString stringWithFormat:@"%.2f", self.yaw],
             @"pitch" : [NSString stringWithFormat:@"%.2f", self.pitch],
             @"roll" : [NSString stringWithFormat:@"%.2f", self.roll],
             @"acceleration" : [NSString stringWithFormat:@"%.2f", self.acceleration],
             @"shotRocketButtonState" : self.shotRocketButtonState ? @"true" : @"false",
             @"timestamp" : [NSString stringWithFormat:@"%d", (int)[[NSDate date] timeIntervalSince1970]]};
}
    
#pragma mark -
#pragma mark Public methods

#pragma mark -
#pragma mark Private methods

@end