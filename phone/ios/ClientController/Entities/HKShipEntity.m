////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  HKShipEntity.m
//  ClientController
//
//  Created by GregoryM on 3/31/13.
//  Copyright 2013 Hackaton. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Imports

#import "HKShipEntity.h"
#import "HKPlayerEntity.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private interface

@interface HKShipEntity ()
@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation

@implementation HKShipEntity

#pragma mark -
#pragma mark Properties

#pragma mark -
#pragma mark Initialization & Release

- (id)initWithDictionary:(NSDictionary *)aDictionary
{
    if (aDictionary == nil)
    {
        NSLog(@"aDictionary is nil");
        return nil;
    }
    self = [super init];
    if (nil != self)
    {
        self.shipId = [[aDictionary objectForKey:@"shipId"] integerValue];
        self.name = [aDictionary objectForKey:@"name"];
        self.occupied = (BOOL)[[aDictionary objectForKey:@"occupied"] integerValue];
        self.playerId = [[aDictionary objectForKey:@"playerId"] integerValue];
    }
    return self;
}

#pragma mark -
#pragma mark Public methods

#pragma mark -
#pragma mark Private methods

@end