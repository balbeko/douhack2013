//
//  HKAppDelegate.h
//  ClientController
//
//  Created by GregoryM on 3/30/13.
//  Copyright (c) 2013 Hackaton. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HKViewController;

@interface HKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *viewController;

@end
