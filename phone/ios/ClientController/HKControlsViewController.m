////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  HKControlsViewController.m
//  ClientController
//
//  Created by GregoryM on 3/31/13.
//  Copyright 2013 Hackaton. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Imports

#import "HKControlsViewController.h"
#import "HKDataService.h"
#import "HKSetPlayerControlStateCommand.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants

static NSUInteger const kMinThrottleViewY = 130;
static NSUInteger const kMaxThrottleViewY = 430;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private interface

@interface HKControlsViewController ()
{
    CMMotionManager *motionManager;
    CMAttitude *referenceAttitude;
    
    NSTimer *pollTimer;
}

- (void)initialize;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation

@implementation HKControlsViewController

#pragma mark -
#pragma mark Properties

#pragma mark -
#pragma mark Initialization & Release

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (nil != self)
    {
        [self initialize];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialize];
}

- (void)initialize
{
    motionManager = [[CMMotionManager alloc] init];
    referenceAttitude = nil;
    
    self.playerStateEntity = [[HKPlayerStateEntity alloc] init];
    self.playerStateEntity.playerId = self.playerEntity.playerId;
}

#pragma mark -
#pragma mark Overriden methods

#pragma mark -
#pragma mark Overriden

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.dataService activate];
    
    [self startDM];
    [self startPolling];
    
    [self calibrateDM];
    
    [self.throttleView addGestureRecognizer:[[UIPanGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(throttleViewPanned:)]];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self stopDM];
    [self stopPolling];
    
    [self.throttleView removeGestureRecognizer:self.throttleView.gestureRecognizers[0]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)accCalibrateClicked:(id)aSender
{
}

- (IBAction)gyroCalibrateClicked:(id)aSender
{
    [self calibrateDM];
}

- (IBAction)fireButtonTouchDown:(id)sender
{
    self.playerStateEntity.shotRocketButtonState = YES;
}

- (IBAction)fireButtonTouchUp:(id)sender
{
    self.playerStateEntity.shotRocketButtonState = NO;
}

- (void)throttleViewPanned:(UIGestureRecognizer *)aGR
{
    CGPoint loc = [aGR locationInView:self.view];
    
    CGRect frame = self.throttleView.frame;
    loc.x = frame.origin.x;
    loc.y = MIN(kMaxThrottleViewY, MAX(kMinThrottleViewY, loc.y));
    loc.y -= frame.size.height / 2;
    NSLog(@"%.2f", loc.y);
    
    frame.origin = loc;
    self.throttleView.frame = frame;
}

#pragma mark -
#pragma mark Requesting

- (void)startPolling
{
    pollTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                 target:self
                                               selector:@selector(poll:)
                                               userInfo:nil
                                                repeats:YES];
}

- (void)poll:(NSTimer*)theTimer
{
    [self sendPlayerControlState];
}

- (void)stopPolling
{
    [pollTimer invalidate];
    pollTimer = nil;
}

#pragma mark -
#pragma mark Gyroscope

- (void)startDM
{
    CMDeviceMotion *deviceMotion = motionManager.deviceMotion;
    referenceAttitude = deviceMotion.attitude;
    
    [motionManager startDeviceMotionUpdates];
}

- (void)calibrateDM
{
    CMDeviceMotion *deviceMotion = motionManager.deviceMotion;
    referenceAttitude = deviceMotion.attitude;
}

- (void)stopDM
{
    [motionManager stopDeviceMotionUpdates];
}

#pragma mark -
#pragma mark Socket delegate

- (void)sendPlayerControlState
{
    CMDeviceMotion *deviceMotion = motionManager.deviceMotion;
    CMAttitude *attitude = deviceMotion.attitude;
    
    if (referenceAttitude != nil)
    {
        [attitude multiplyByInverseOfAttitude:referenceAttitude];
    }
    
    self.playerStateEntity.yaw = MIN(MAX(-1, attitude.yaw * 2 / M_PI), 1);
    self.playerStateEntity.pitch = MIN(MAX(-1, attitude.pitch * 2 / M_PI), 1);
    self.playerStateEntity.roll = MIN(MAX(-1, attitude.roll * 2 / M_PI), 1);
    
    self.playerStateEntity.acceleration = -(2 * ((CGRectGetMidY(self.throttleView.frame) - (CGFloat)kMinThrottleViewY) /
                                               (CGFloat)(kMaxThrottleViewY - kMinThrottleViewY)) - 1);
    
    NSLog(@"sending state");
    HKSetPlayerControlStateCommand *stateCommand = [[HKSetPlayerControlStateCommand alloc] init];
    stateCommand.stateEntity = self.playerStateEntity;
    
    [stateCommand sendCommandInSocket:self.dataService.socketIO withBlock:^(HKClientCommand *command) {
        NSLog(@"state processed");
    }];
}

@end