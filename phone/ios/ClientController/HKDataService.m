////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  HKDataService.m
//  ClientController
//
//  Created by GregoryM on 3/31/13.
//  Copyright 2013 Hackaton. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Imports

#import "HKDataService.h"
#import "HKClientCommand.h"
#import "HKGetAvailableShipsCommand.h"
#import "HKBindWithShipCommand.h"
#import "HKSetPlayerControlStateCommand.h"
#import "HKServerCommand.h"
#import "HKProvideFeedbackServerCommand.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants

static NSString * const kHostURL = @"172.29.8.117";
static NSInteger const kHostPort = 9999;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private interface

@interface HKDataService ()
{
    NSTimer *pollTimer;
}

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation

@implementation HKDataService

#pragma mark -
#pragma mark Properties

#pragma mark -
#pragma mark Initialization & Release

- (id)init
{
    self = [super init];
    if (nil != self)
    {
        self.active = NO;
        self.socketIO = [[SocketIO alloc] initWithDelegate:self];
    }
    return self;
}

#pragma mark -
#pragma mark Overriden

- (void)activate
{
    if (!self.active)
    {
        [self.socketIO connectToHost:kHostURL onPort:kHostPort];
        
        self.active = YES;
    }
}

- (void)deactivate
{
    if (self.active)
    {
        [self.socketIO disconnect];
        
        self.active = NO;
    }
}

#pragma mark -
#pragma mark Socket delegate

- (void)socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet
{
    NSLog(@"socket received event, name %@, data %@", packet.name, packet.data);
    
    HKServerCommand *command = [HKServerCommand commandForName:packet.name];
    if (command != nil)
    {
        if ([command isKindOfClass:[HKProvideFeedbackServerCommand class]])
        {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            return;
        }
    }
    
    NSLog(@"unrecognized server command");
}

- (void)socketIO:(SocketIO *)socket failedToConnectWithError:(NSError *)error
{
    NSLog(@"socket failed, error %@", [error localizedDescription]);
}

@end