////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  HKShipSelectionViewController.h
//  ClientController
//
//  Created by GregoryM on 3/31/13.
//  Copyright (c) 2013 Hackaton. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Imports

#import "HKDataService.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Predeclarations

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Interface

@interface HKShipSelectionViewController : UIViewController

@property (nonatomic, retain) HKDataService *dataService;

@property (nonatomic, assign) IBOutlet UIButton *ship1Button;
@property (nonatomic, assign) IBOutlet UIButton *ship2Button;

@property (nonatomic, assign) IBOutlet UIView *overlayView;
@property (nonatomic, assign) IBOutlet UILabel *overlayLabel;
@property (nonatomic, assign) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)ship1ButtonClicked:(id)sender;
- (IBAction)ship2ButtonClicked:(id)sender;

@end
