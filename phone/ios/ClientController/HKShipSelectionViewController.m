////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  HKShipSelectionViewController.m
//  ClientController
//
//  Created by GregoryM on 3/31/13.
//  Copyright 2013 Hackaton. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Imports

#import "HKShipSelectionViewController.h"
#import "HKControlsViewController.h"
#import "HKDataService.h"
#import "HKShipEntity.h"

#import "HKClientCommand.h"
#import "HKGetAvailableShipsCommand.h"
#import "HKBindWithShipCommand.h"
#import "HKSetPlayerControlStateCommand.h"
#import "HKServerCommand.h"
#import "HKProvideFeedbackServerCommand.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private interface

@interface HKShipSelectionViewController ()

- (void)initialize;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation

@implementation HKShipSelectionViewController

#pragma mark -
#pragma mark Properties

#pragma mark -
#pragma mark Initialization & Release

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (nil != self)
    {
        [self initialize];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialize];
}

- (void)initialize
{
    self.dataService = [[HKDataService alloc] init];
}

#pragma mark -
#pragma mark Overriden methods

- (void)viewDidLoad
{
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.dataService activate];
    
    self.overlayView.hidden = NO;
    [self.activityIndicator startAnimating];
    
    HKGetAvailableShipsCommand *shipCommand = [[HKGetAvailableShipsCommand alloc] init];
    [shipCommand sendCommandInSocket:self.dataService.socketIO withBlock:^(HKClientCommand *command)
     {
         if (command.executed)
         {
             NSArray *ships = [(HKGetAvailableShipsCommand *)command ships];
             if (((HKShipEntity *)ships[0]).occupied)
             {
                 self.ship1Button.enabled = NO;
             }
             if (((HKShipEntity *)ships[1]).occupied)
             {
                 self.ship2Button.enabled = NO;
             }
         }
         
         dispatch_async(dispatch_get_main_queue(), ^{
             if (command.executed)
             {
                 self.overlayView.hidden = YES;
                 [self.activityIndicator stopAnimating];
             }
             else
             {
                 self.overlayLabel.text = @"Error occured";
                 self.activityIndicator.hidden = YES;
             }
         });
    }];
}

- (void)viewDidDisappear:(BOOL)animated
{
}

#pragma mark -
#pragma mark Public methods

- (IBAction)ship1ButtonClicked:(id)sender
{
    HKBindWithShipCommand *shipCommand = [[HKBindWithShipCommand alloc] init];
    shipCommand.shipId = 0;
    shipCommand.playerName = @"PlayerForShip1";
    
    [self bindShipWithCommand:shipCommand];
}

- (IBAction)ship2ButtonClicked:(id)sender
{
    HKBindWithShipCommand *shipCommand = [[HKBindWithShipCommand alloc] init];
    shipCommand.shipId = 1;
    shipCommand.playerName = @"PlayerForShip2";
    
    [self bindShipWithCommand:shipCommand];
}

- (void)bindShipWithCommand:(HKBindWithShipCommand *)shipCommand
{
    [shipCommand sendCommandInSocket:self.dataService.socketIO withBlock:^(HKClientCommand *command)
     {
         HKBindWithShipCommand *_command = (HKBindWithShipCommand *)command;
         if (_command.executed && _command.result)
         {
             HKPlayerEntity *player = [[HKPlayerEntity alloc] init];
             player.playerId = _command.playerId;
             player.name = _command.playerName;
             player.shipId = _command.shipId;
             
             HKControlsViewController *controller = [[HKControlsViewController alloc] init];
             controller.playerEntity = player;
             controller.dataService = self.dataService;
             [self.navigationController pushViewController:controller animated:NO];
         }
         else
         {
             NSLog(@"error occured");
         }
     }];
}

#pragma mark -
#pragma mark Private methods

@end