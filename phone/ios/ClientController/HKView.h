////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  HKView.h
//  ClientController
//
//  Created by GregoryM on 3/30/13.
//  Copyright (c) 2013 Hackaton. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Imports

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Predeclarations

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Interface

@interface HKView : UIView

// Coordinates should be from -0.5 to 0.5
@property (assign) CGPoint centerPoint;

@end
