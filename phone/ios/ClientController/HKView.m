////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  HKView.m
//  ClientController
//
//  Created by GregoryM on 3/30/13.
//  Copyright 2013 Hackaton. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Imports

#import "HKView.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private interface

@interface HKView ()
@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation

@implementation HKView

#pragma mark -
#pragma mark Properties

#pragma mark -
#pragma mark Initialization & Release

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
    }
    return self;
}

#pragma mark -
#pragma mark Overriden methods

- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(ctx, [[UIColor blackColor] CGColor]);
    CGContextSetFillColorWithColor(ctx, [[UIColor blackColor] CGColor]);
    
    CGContextStrokeRect(ctx, rect);
    
    CGContextBeginPath(ctx);
    CGContextMoveToPoint(ctx, CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGContextAddLineToPoint(ctx, CGRectGetMidX(rect), CGRectGetMaxY(rect));
    CGContextMoveToPoint(ctx, CGRectGetMinX(rect), CGRectGetMidY(rect));
    CGContextAddLineToPoint(ctx, CGRectGetMaxX(rect), CGRectGetMidY(rect));

    CGContextStrokePath(ctx);
    
    CGRect circleRect = CGRectMake(0, 0, 20, 20);
    circleRect.origin.x = ((-self.centerPoint.x + 0.5) * rect.size.width - circleRect.size.width / 2);
    circleRect.origin.y = ((-self.centerPoint.y + 0.5) * rect.size.height - circleRect.size.height / 2);
    
    CGContextFillEllipseInRect(ctx, circleRect);
}

#pragma mark -
#pragma mark Public methods

#pragma mark -
#pragma mark Private methods

@end