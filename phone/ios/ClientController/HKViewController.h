//
//  HKViewController.h
//  ClientController
//
//  Created by GregoryM on 3/30/13.
//  Copyright (c) 2013 Hackaton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HKView.h"
#import "SocketIO.h"
#import "SocketIOPacket.h"

@interface HKViewController : UIViewController <SocketIODelegate>
{
}

@property (assign) IBOutlet UILabel *accXLabel;
@property (assign) IBOutlet UILabel *accYLabel;
@property (assign) IBOutlet UILabel *accZLabel;

@property (assign) IBOutlet UILabel *gyroXLabel;
@property (assign) IBOutlet UILabel *gyroYLabel;
@property (assign) IBOutlet UILabel *gyroZLabel;

@property (assign) IBOutlet HKView *controlView;

- (IBAction)accCalibrateClicked:(id)aSender;
- (IBAction)gyroCalibrateClicked:(id)aSender;

@end
