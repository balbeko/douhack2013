//
//  HKViewController.m
//  ClientController
//
//  Created by GregoryM on 3/30/13.
//  Copyright (c) 2013 Hackaton. All rights reserved.
//

#import "HKViewController.h"
#import "HKClientCommand.h"
#import "HKGetAvailableShipsCommand.h"
#import "HKBindWithShipCommand.h"
#import "HKSetPlayerControlStateCommand.h"
#import "HKServerCommand.h"
#import "HKProvideFeedbackServerCommand.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// COnstants

//static NSString * const kHostURL = @"172.29.8.117";
//static NSInteger const kHostPort = 9999;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private

@interface HKViewController ()
{
    CMMotionManager *motionManager;
    CMAttitude *referenceAttitude;
    
    NSTimer *pollTimer;
}

@property (retain) SocketIO *socketIO;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Impl

@implementation HKViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])
    {
        motionManager = [[CMMotionManager alloc] init];
        referenceAttitude = nil;
        
        self.socketIO = [[SocketIO alloc] initWithDelegate:self];
    }
    return self;
}

#pragma mark -
#pragma mark Overriden

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    //[self.socketIO connectToHost:kHostURL onPort:kHostPort];
    
    [self startDM];
    [self startPolling];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.socketIO disconnect];
    
    [self stopDM];
    [self stopPolling];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)accCalibrateClicked:(id)aSender
{
}

- (IBAction)gyroCalibrateClicked:(id)aSender
{
    [self calibrateDM];
}

#pragma mark -
#pragma mark Requesting

- (void)startPolling
{
    pollTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                 target:self
                                               selector:@selector(poll:)
                                               userInfo:nil
                                                repeats:YES];
}

- (void)poll:(NSTimer*)theTimer
{
    [self readDMValues];
    [self updateControlView];
    [self setDataToSocket];
}

- (void)stopPolling
{
    [pollTimer invalidate];
    pollTimer = nil;
}

#pragma mark -
#pragma mark Gyroscope

- (void)startDM
{
    CMDeviceMotion *deviceMotion = motionManager.deviceMotion;
    referenceAttitude = deviceMotion.attitude;
    //[motionManager startGyroUpdates];
    [motionManager startDeviceMotionUpdates];
}

- (void)calibrateDM
{
    CMDeviceMotion *deviceMotion = motionManager.deviceMotion;
    referenceAttitude = deviceMotion.attitude;
}

- (void)stopDM
{
    //[motionManager stopGyroUpdates];
    [motionManager stopDeviceMotionUpdates];
}

- (void)readDMValues
{
    CMDeviceMotion *deviceMotion = motionManager.deviceMotion;
    CMAttitude *attitude = deviceMotion.attitude;
    
    if (referenceAttitude != nil)
    {
        [attitude multiplyByInverseOfAttitude:referenceAttitude];
    }
    
    //NSLog(@"%.2f, %.2f, %.2f", attitude.pitch, attitude.yaw, attitude.roll);
    self.gyroXLabel.text = [NSString stringWithFormat:@"%.0fº", attitude.pitch * 180.0 / M_PI];
    self.gyroYLabel.text = [NSString stringWithFormat:@"%.0fº", attitude.yaw * 180.0 / M_PI];
    self.gyroZLabel.text = [NSString stringWithFormat:@"%.0fº", attitude.roll * 180.0 / M_PI];
    
    const float gConst = 0.981;
    
    self.accXLabel.text = [NSString stringWithFormat:@"%.2fº", deviceMotion.gravity.x / gConst];
    self.accYLabel.text = [NSString stringWithFormat:@"%.2fº", deviceMotion.gravity.y / gConst];
    self.accZLabel.text = [NSString stringWithFormat:@"%.2fº", deviceMotion.gravity.z / gConst];
}

- (void)updateControlView
{
    CMDeviceMotion *deviceMotion = motionManager.deviceMotion;
    CMAttitude *attitude = deviceMotion.attitude;
    
    if (referenceAttitude != nil)
    {
        [attitude multiplyByInverseOfAttitude:referenceAttitude];
    }
    
    self.controlView.centerPoint = CGPointMake(MIN(MAX(-0.5, attitude.yaw / M_PI), 0.5),
                                               MIN(MAX(-0.5, attitude.pitch / M_PI), 0.5));
    [self.controlView setNeedsDisplay];
}

#pragma mark -
#pragma mark Socket delegate

- (void)socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet
{
    NSLog(@"socket received event, name %@, data %@", packet.name, packet.data);
    
    HKServerCommand *command = [HKServerCommand commandForName:packet.name];
    if (command != nil)
    {
        if ([command isKindOfClass:[HKProvideFeedbackServerCommand class]])
        {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            return;
        }
    }
    
    NSLog(@"unrecognized server command");
}

- (void)socketIO:(SocketIO *)socket failedToConnectWithError:(NSError *)error
{
    NSLog(@"socket failed, error %@", [error localizedDescription]);
}

- (void)setDataToSocket
{
    CMDeviceMotion *deviceMotion = motionManager.deviceMotion;
    CMAttitude *attitude = deviceMotion.attitude;
    
    if (referenceAttitude != nil)
    {
        [attitude multiplyByInverseOfAttitude:referenceAttitude];
    }
    
    NSDictionary *data = @{@"x": [NSString stringWithFormat:@"%.2f", MIN(MAX(-0.5, attitude.yaw / M_PI), 0.5)],
                           @"y": [NSString stringWithFormat:@"%.2f", MIN(MAX(-0.5, attitude.pitch / M_PI), 0.5)],
                           @"timestamp": [NSString stringWithFormat:@"%d", (int)[[NSDate date] timeIntervalSince1970]]};
    
    NSLog(@"sending data: %@", data);
    
    [self.socketIO sendEvent:@"updateValues" withData:data andAcknowledge:^(id argsData)
     {
         NSDictionary *response = argsData;
         NSLog(@"ack arrived: %@", response);
     }];
}

@end
