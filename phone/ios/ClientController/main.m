//
//  main.m
//  ClientController
//
//  Created by GregoryM on 3/30/13.
//  Copyright (c) 2013 Hackaton. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HKAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HKAppDelegate class]));
    }
}
