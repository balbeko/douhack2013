app    = require('express')()
server = require('http').createServer app
io     = require('socket.io').listen server
util   = require 'util'
debug  = util.inspect
server.listen 9999

app.get '/', (req, res)->
  res.sendfile(__dirname + '/index.html')

io.configure 'prod', ()->
  io.set 'log level', 0

io.configure 'dev', ()->
  io.set 'log level', 1

io.configure 'debug', ()->
  io.set 'log level', 3
## ========================



class Player
  constructor: (name, id) ->
    @name = name
    @socketId = id

class Ship
  constructor: (id, name) ->
    @name   = name
    @player = null
    @id     = id

  isFree: ()->
    not @player?

  bindWithPlayer: (player)->
    if @player?
      return false
    else
      @player = player
      return true
  free: ()->
    @player = null

  toJSON: ()->
    data =
      shipId: @id
      name: @name
      occupied: @player?
    return data

class State
  constructor: (player)->
    @player = player
    @pitch  = 0
    @yaw    = 0
    @roll   = 0
    @accel  = 0
    @state  = false



ship1 = new Ship(0, "Ship1")
ship2 = new Ship(1, "Ship2")

ships = [ship1, ship2]
players = {}

io.sockets.on 'connection', (socket) ->
  console.log "Connected #{socket.id}"

  socket.on 'getAvailableShips', (data, fn) ->
    response = []
    for ship in ships
      response.push ship.toJSON()
    fn(JSON.stringify({ships: response}))
## -----------------------
  socket.on "bindWithShip", (data, fn) ->
    console.log "bindWithShip: shipId=#{data.shipId}, playerName=#{data.playerName}"
    debugger
    currentPlayer = new Player(data.playerName, socket.id)
    players[socket.id] = currentPlayer
    res = ships[parseInt(data.shipId)].bindWithPlayer(currentPlayer)
    if res
      msg = "ok"
    else
      msg = "error"

    response =
      result: msg

    fn(JSON.stringify(response))
## -----------------------
  socket.on "setPlayerControlState", (data) ->
    console.log "setPlayerControlState: playerId=#{data.playerId}, yaw=#{data.yaw}, pitch=#{data.pitch}, roll=#{data.roll}, accel=#{data.acceleration} shotRocketButtonState=#{data.shotRocketButtonState}"
    socket.broadcast.emit "playerControlState", data
## -----------------------
  #socket.emit "provideFeedback", event: "vibrate"

  socket.on 'disconnect', ()->
    for ship in ships
      if ship.player == players[socket.id]
        ship.free()
        delete(players[socket.id])


    console.log "Disconnected #{socket.id}"




#################################################


    #







